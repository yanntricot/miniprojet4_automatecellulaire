import random

def Get_Taille_carte(carte):
    """Permet de renvoyer la taille du tableau de 2d fournis

    Arguments:
    lignes -- Nombre de lignes de la carte à initialiser;
    """
    return (len(carte)), (len(carte[0]))

def init_Carte(lignes, colonnes, probaForet):  
    """Initialisation de la carte

    Arguments:
    lignes -- Nombre de lignes de la carte à initialiser;
    colonnes -- Nombre de colonnes de la carte à initialiser;
    probaForet -- Pourcentage de forêt sur la carte;
    """

    # 0 = rien, 1 = cendres, 2 = arbre en feu, 3 = arbre
    carte = [[0 for colonne in range(colonnes)] for ligne in range(lignes)] 
    init_Foret(lignes, colonnes, probaForet, carte)
    return carte

def init_Foret(lignes, colonnes, probaForet, carte): 
    """Initialisation de la forêt

    Arguments:
    lignes -- Nombre de lignes de la carte à initialiser;
    colonnes -- Nombre de colonnes de la carte à initialiser;
    probaForet -- Pourcentage de forêt sur la carte;
    carte -- Carte actuelle a modifier;
    """

    for positionLigne in range(lignes):
    	for positionColonne in range(colonnes):
    		if random.random() <= probaForet:
    			carte[positionLigne][positionColonne]=3


def propagation_Feu_Un_Tour(carte, typePropagation): 
    """Permet de Propager le feu après une heure
    
    Argument:
    carte -- Carte actuelle a modifier;
    typePropagation -- spécifie le type de régle a suivre;
    """
    lignes, colonnes = Get_Taille_carte(carte)
    emplacementUtilise = [[1 for colonne in range(colonnes)] for ligne in range(lignes)]
    mouvement_Feu_Carte(carte, emplacementUtilise, typePropagation)

def mouvement_Feu_Carte(carte, emplacementUtilise, typePropagation):
    """Permet de se déplacer dans toutes les cases de la carte
    
    Arguments:
    carte -- Carte actuelle a modifier;
    emplacementUtilise -- Carte de booléan réprésantant si l'unité a été utilisé
    typePropagation -- spécifie le type de régle a suivre;
    """
    lignes, colonnes = Get_Taille_carte(carte)
    for positionLigne in range(lignes):
        for positionColonne in range(colonnes):
            if emplacementUtilise[positionLigne][positionColonne]==1:
                action_Position(positionLigne, positionColonne, carte, emplacementUtilise, typePropagation)

def action_Position(positionLigne, positionColonne, carte, emplacementUtilise, typePropagation):
    """Lance le code requis en fonction de l'êtat de la case obtenu par les coordonnées position

    Arguments:
    positionLigne -- Coordonnée Horizontal a modifier;
    positionColonne -- Coordonnée Vertical a modifier;
    carte -- Carte actuelle a modifier;
    emplacementUtilise -- Carte de booléan réprésantant si l'unité a été utilisé
    typePropagation -- spécifie le type de régle a suivre;
    """

    if carte[positionLigne][positionColonne] == 2:
        propagation_Feu_VonNeumann(positionLigne, positionColonne, carte, emplacementUtilise, typePropagation)
    if carte[positionLigne][positionColonne] == 1 or carte[positionLigne][positionColonne] == 2:
        changement_Etat(positionLigne, positionColonne, carte, emplacementUtilise, typePropagation)
    if carte[positionLigne][positionColonne] < 3:
        emplacementUtilise[positionLigne][positionColonne] = 0

def propagation_Feu_VonNeumann(positionLigne, positionColonne, carte, emplacementUtilise, typePropagation):
    """Permet la propagation du feu à la manière Von NeuMann sur une case fournie

    Arguments:
    positionLigne -- Coordonnée Horizontal a modifier;
    positionColonne -- Coordonnée Vertical a modifier;
    carte -- Carte actuelle a modifier;
    emplacementUtilise -- Carte de booléan réprésantant si l'unité a été utilisé
    typePropagation -- spécifie le type de régle a suivre;
    """
    lignes, colonnes = Get_Taille_carte(carte)
    if positionLigne-1>=0:
        propagation_Feu(positionLigne-1, positionColonne, carte, emplacementUtilise, typePropagation)
    if positionLigne+1<lignes:
        propagation_Feu(positionLigne+1, positionColonne, carte, emplacementUtilise, typePropagation)
    if positionColonne-1>=0:
        propagation_Feu(positionLigne, positionColonne-1, carte, emplacementUtilise, typePropagation)
    if positionColonne+1<colonnes:
        propagation_Feu(positionLigne, positionColonne+1, carte, emplacementUtilise, typePropagation)	

def propagation_Feu(positionLigne, positionColonne, carte, emplacementUtilise, typePropagation):
    """Change l'état de la case si les conditions sont remplies 

    Arguments:
    positionLigne -- Coordonnée Horizontal a modifier;
    positionColonne -- Coordonnée Vertical a modifier;
    carte -- Carte actuelle a modifier;
    emplacementUtilise -- Carte de booléan réprésantant si l'unité a été utilisé
    typePropagation -- spécifie le type de régle a suivre;
    """

    if emplacementUtilise[positionLigne][positionColonne] == 1:
        changement_Etat(positionLigne, positionColonne, carte, emplacementUtilise, typePropagation)
        emplacementUtilise[positionLigne][positionColonne] = 0

def changement_Etat(positionLigne, positionColonne, carte, emplacementUtilise, typePropagation):
    """Change l'état de la case en fonction du type de propagation demandée

    Arguments:
    positionLigne -- Coordonnée Horizontal a modifier;
    positionColonne -- Coordonnée Vertical a modifier;
    carte -- Carte actuelle a modifier;
    typePropagation -- spécifie le type de régle a suivre;
    """
    if carte[positionLigne][positionColonne] > 0:
        if typePropagation == True:
            if carte[positionLigne][positionColonne] == 3:
                if extansion_Feu(positionLigne, positionColonne, carte, emplacementUtilise):
                    carte[positionLigne][positionColonne]=(carte[positionLigne][positionColonne]-1)
            else:
                carte[positionLigne][positionColonne]=(carte[positionLigne][positionColonne]-1)
        else: 
            carte[positionLigne][positionColonne]=(carte[positionLigne][positionColonne]-1)


def affichage_Carte(carte):
    """Affiche la carte fournie

    Arguments:
    carte -- Carte actuelle a modifier;
    """

    for ligne in carte:
        print(ligne) 

def init_Feu(positionLigne, positionColonne, carte):
    """Change l'état d'une case en feu

    Arguments:
    positionLigne -- Coordonnée Horizontal a modifier;
    positionColonne -- Coordonnée Vertical a modifier;
    carte -- Carte actuelle a modifier;
    """
    lignes, colonnes = Get_Taille_carte(carte)
    if positionLigne>=0 and positionLigne<lignes:
        if positionColonne>=0 and positionColonne<colonnes:
            carte[positionLigne][positionColonne]=2

def feu_Present(etatActuel, dejaModifie):
    """Renvois un boolean si le feu est sur cette case lors du tour précédent

    Arguments:
    etatActuel -- l'etat actuel de la case (int)
    dejaModifier -- boolean affirmant si oui ou non, la case a déjà était modifiée
    """
    if dejaModifie == 1 and etatActuel == 2:
        return True
    return False

def comptage_Feu_Environant_Horizontal(positionLigne, positionColonne, carte, emplacementUtilise,compteur):
    """Renvois le nombre de feu pour les positions gauche et droite de la case à étudier

    Arguments:
    positionLigne -- Coordonnée Horizontal a modifier;
    positionColonne -- Coordonnée Vertical a modifier;
    carte -- Carte actuelle a modifier;
    emplacementUtilise -- Carte de booléan réprésantant si l'unité a été utilisé
    compteur -- nombre de feu déjà existant
    """
    lignes, colonnes = Get_Taille_carte(carte)
    if positionLigne-1>=0:
        if feu_Present(carte[positionLigne-1][positionColonne], emplacementUtilise[positionLigne-1][positionColonne]):
            compteur=compteur+1 
    if positionLigne+1<lignes:
        if feu_Present(carte[positionLigne+1][positionColonne], emplacementUtilise[positionLigne+1][positionColonne]):
            compteur=compteur+1 
    return compteur

def comptage_Feu_Environant_Vertical(positionLigne, positionColonne, carte, emplacementUtilise,compteur):
    """Renvois le nombre de feu pour les positions haut et bas de la case à étudier

    Arguments:
    positionLigne -- Coordonnée Horizontal a modifier;
    positionColonne -- Coordonnée Vertical a modifier;
    carte -- Carte actuelle a modifier;
    emplacementUtilise -- Carte de booléan réprésantant si l'unité a été utilisé
    compteur -- nombre de feu déjà existant
    """
    lignes, colonnes = Get_Taille_carte(carte)
    if positionColonne-1>=0:
        if feu_Present(carte[positionLigne][positionColonne-1], emplacementUtilise[positionLigne][positionColonne-1]):
            compteur=compteur+1 
    if positionColonne+1<colonnes:
        if feu_Present(carte[positionLigne][positionColonne+1], emplacementUtilise[positionLigne][positionColonne+1]):
            compteur=compteur+1 
    return compteur

def comptage_Feu_Environant(positionLigne, positionColonne, carte, emplacementUtilise):
    """Renvois le nombre de feu pour toutes les position voisine à case à étudier

    Arguments:
    positionLigne -- Coordonnée Horizontal a modifier;
    positionColonne -- Coordonnée Vertical a modifier;
    carte -- Carte actuelle a modifier;
    emplacementUtilise -- Carte de booléan réprésantant si l'unité a été utilisé
    """
    compteur = 0
    compteur = comptage_Feu_Environant_Horizontal(positionLigne, positionColonne, carte, emplacementUtilise, compteur)
    compteur = comptage_Feu_Environant_Vertical(positionLigne, positionColonne, carte, emplacementUtilise,compteur)
    return compteur

def extansion_Feu(positionLigne, positionColonne, carte, emplacementUtilise):
    """Renvois un booléan si oui ou non le feu est propagé selon le nombre de feu autour de lui

    Arguments:
    positionLigne -- Coordonnée Horizontal a modifier;
    positionColonne -- Coordonnée Vertical a modifier;
    carte -- Carte actuelle a modifier;
    emplacementUtilise -- Carte de booléan réprésantant si l'unité a été utilisé
    """
    compteur = comptage_Feu_Environant(positionLigne, positionColonne, carte, emplacementUtilise)
    if random.random() <= (1-(1/(compteur+1))):
    	return True
    return False