from tkinter import *
from script import *
from tkinter.messagebox import *
import datetime
import argparse

# Instanciation d'une fenêtre Tkinter
master = Tk()
master.title("Simulateur feu de forêt")

class Range(object):
    def __init__(self, start, end):
        self.start = start
        self.end = end
    def __eq__(self, other):
        return self.start <= other <= self.end

# Permet le passage d'arguments lors de l'appel du script
parser = argparse.ArgumentParser(description="Interface d'automate cellulaire basique")
parser.add_argument("nombreLignes", help="Ecrire le nombre de lignes de la carte", type=int,nargs='?', default=25)
parser.add_argument("nombreColonnes", help="Ecrire le nombre de colonnes de la carte", type=int,nargs='?', default=25)
parser.add_argument("dureAnimation", help="Ecrire la durée entre chaque animation", type=int,nargs='?', default=1)
parser.add_argument("tailleCellule", help="Ecrire la taille de chaque cellules", type=int,nargs='?', default=20)
parser.add_argument("probaForet", help="Ecrire la probabilité de génération d'une forêt", type=float,nargs='?', choices=[Range(0.0, 1.0)], default=0.4)
args = parser.parse_args()

# Variables passées en paramètre dans l'appel prog
nb_row = args.nombreLignes
nb_col = args.nombreColonnes
cell_size = args.tailleCellule
prob_f = args.probaForet
duree_anim = args.dureAnimation * 1000 # Multiplication par 1000 pour obtenir temps en millisecondes

# Variables propre à l'interface et sa connectique avec le backend
pause = True
str_pause = StringVar()
str_pause.set("Pause activée")
regle2_chk = IntVar()

# Couleurs utilisées pour la simulation
color_vide   = "#6B3F35"
color_cendre = 'grey'
color_foret  = "#1d7c25"
color_feu    = "#c03018"

# Initialisation de la carte 
carte = init_Carte(nb_row, nb_col,prob_f)

# Variables calculées en fct des variables précédentes
canvas_width  = nb_col*cell_size
canvas_height = nb_row*cell_size

def actu_Carte():
    """
    FCT - actu_Carte
    ================
    IN :[/]
    OUT:[/] Actualisation de la carte

    Cette fonction permet d'actualiser la carte ligne par ligne colonne par colonne
    en fonction de l'état de la case nous lui attribuons une couleur correspondante.
    """
    for i in range(nb_row):
        for j in range(nb_col):
            if(carte[i][j]==0):
                canvas.create_rectangle(cell_size*j, cell_size*i, cell_size*j+cell_size, cell_size*i+cell_size, fill=color_vide)
            elif(carte[i][j]==1):
                canvas.create_rectangle(cell_size*j, cell_size*i, cell_size*j+cell_size, cell_size*i+cell_size, fill=color_cendre)
            elif(carte[i][j]==2):
                canvas.create_rectangle(cell_size*j, cell_size*i, cell_size*j+cell_size, cell_size*i+cell_size, fill=color_feu)
            else:
                canvas.create_rectangle(cell_size*j, cell_size*i, cell_size*j+cell_size, cell_size*i+cell_size, fill=color_foret)

# CallBack pour le bouton 'Launch'
def launch_callback():
    global pause
    global str_pause
    pause = False
    str_pause.set("Pause désactivée")
    anim()

# CallBack pour le bouton 'Pause'
def pause_callback():
    global pause 
    global str_pause
    pause = True
    str_pause.set("Pause activée")
    anim()

# PopUp affichant la RègleN°2
def info_callback():
    showinfo('Informations', 'Règle N°2 > \n========\nUne cellule à x pourcent de chance de prendre feu à t+1 selon la formule 1 - (1/k+1) où k = nombre de voisins en feu.')

def anim():
    """
    FCT - anim
    ==========
    IN :[/]
    OUT:[/] Permet l'animation du feu de forêt

    Cette fonction permet de gérer l'animation du feu de forêt soit, l'affichage de la 
    carte, la mise en pause, la pose d'abre ou de feu, le choix de la règle et le 
    lancement de la simulation.
    """
    global pause
    if(pause == False):
        if(regle2_chk.get() == 0):
            regle2 = False
        else:
            regle2 = True
        propagation_Feu_Un_Tour(carte, regle2)
        actu_Carte()
        master.after(duree_anim, anim)
    
## Instanciation du Canvas
#=======================================================================
l = LabelFrame(master, text="Automate cellulaire", padx=20, pady=20)
canvas = Canvas(l, width=canvas_width , height=canvas_height)

l.pack(fill="both", expand="yes")
canvas.pack()
#=======================================================================
lbl_pause = Label(master, textvar=str_pause)
button_launch = Button(master, text ="Launch", command = launch_callback)
button_pause = Button(master, text ="Pause", command = pause_callback)
button_info = Button(master, text ="Infos", command = info_callback)
chk_btn = Checkbutton(master, text="Activer Règle N°2", variable = regle2_chk)

lbl_pause.pack()
button_launch.pack(side = LEFT, padx=20, pady=20)
button_pause.pack(side = RIGHT, padx=20, pady=20)
chk_btn.pack()
button_info.pack()
#=======================================================================

# Permet gestion des clics gauches et droits
def click_callback(event):
    x1 = event.x # coord x du click
    y1 = event.y # coord y du click

    x = int(x1/cell_size) # recup case clické en x
    y = int(y1/cell_size) # recup case clické en y
    if(pause == True):
        # Si on click gauche
        if event.num == 1:
            color = color_feu
            carte[y][x] = 2

        # Si on click droit
        elif event.num == 3:
            color = color_foret
            carte[y][x] = 3

        # On recréait un carré à la même place mais avec son nouvel état
        canvas.create_rectangle(cell_size*x, cell_size*y, cell_size*x+cell_size, cell_size*y+cell_size, fill=color)
    
#Binding des touches sur le canvas
canvas.bind('<Button-1>', click_callback)
canvas.bind('<Button-3>', click_callback)

# Main permetant d'actualiser dans un premier temps la carte pour avoir un 1er rendu
if __name__ == "__main__":
    actu_Carte()
    master.mainloop()